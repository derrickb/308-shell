/*
Derrick Brandt
03/04/22
3.2 Signal Handlers
*/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void my_routine(int signo);

int main() {
    //alter the disposition of the SIGINT signal and SIGQUIT signal
    signal(SIGINT, my_routine);
    signal(SIGQUIT, my_routine);
    printf("Entering infinite loop\n");

    while(1) { 
        sleep(10); 
    }

    printf("Can’t get here\n");
}
        
void my_routine(int signo) {
    printf("The signal number is %d.\n", signo);
}

/* Q.E.D */