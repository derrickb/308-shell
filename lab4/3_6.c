/*
Derrick Brandt
3.6
03/04/22
Pipes
*/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

#define MESSAGESIZE 16

int main() {
    char *msg = "How are you?";
    char inbuff[MESSAGESIZE];
    int p[2];
    int ret;
    pipe(p);

    ret = fork( );
    if (ret > 0) {
        write(p[1], msg, MESSAGESIZE);
    } 
    else {
        sleep(1);
        read(p[0], inbuff, MESSAGESIZE);
        printf("%s\n", inbuff);
    }
    
    exit(0);
}