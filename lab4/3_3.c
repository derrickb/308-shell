/*
Derrick Brandt
03/04/22
Signals for Exceptions
*/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

void my_routine(int standardizedSignalNumber);

int main(void){

    signal(SIGFPE, my_routine);

    int a = 4;
    a = a/0;
}

void my_routine(int standardizedSignalNumber){
    printf("error caught - signal number: %d\n", standardizedSignalNumber);
    exit(1);
}

