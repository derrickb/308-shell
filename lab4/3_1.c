/*
Derrick Brandt
Lab 4
Exercise 3.1 Introduction to Signals
*/

#include <signal.h>
#include <stdio.h>
#include <unistd.h>

//declare function
void my_routine( );

int main( ) {

    signal(SIGINT, SIG_IGN);
    printf("Entering infinite loop\n");

    while(1) {
        sleep(10);
    } /* take an infinite number of naps */
    
    printf("Can't get here\n");
}

void my_routine(void){
    printf("Running my_routine\n");
}