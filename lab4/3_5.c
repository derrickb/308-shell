/*
Derrick Brandt
Lab 4
03/04/22
Signals with Fork
*/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

void my_routine();
int ret;

int main (){
    ret = fork( );
    signal(SIGINT, my_routine);
    printf("Entering infinite loop\n");
    while(1){ 
        sleep(10);
    }
    printf("Can't get here\n");
}

void my_routine(){
    printf("Return value from fork = %d\n", ret);
}