/* 
Derrick Brandt 
CPRE 308
Due March 3rd
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#define INFINITE 1
#define COMMANDSIZE 256
#define MAXLENGTH 512

// declare functions
void runBuiltInCommand(char** commandString, int numInputs);
void runProgramCommand(char** commandString);
void freeStringArray(char** commandStringArray);
void prepareBackgroundExecution(char** commandString, int numInputs);
void executeBackgroundThread(char** commandString); 


int main(int argc, char** argv){

    // declare variables
    char* prompt = "308sh> ";
    char commandUserEnters[COMMANDSIZE];
    char** parsed_command = malloc(1); 
    //chunk/cut the string by spaces
    const char delimiter[2] = " "; 
    char* token;
    int i,j = 0;
    int stringLength;

    // conditioinally check if user wants personal prompt
    if(argc != 1){
        // string compare: -1, 0 if they are equal, 1 if they are greater than
        if(strcmp(argv[1], "-p") == 0){
            prompt = argv[2];
        }
    }

    // infinitely loop
    while(INFINITE){

        // properly initialize each iteration
        parsed_command = malloc(1); 
        i = 0;
        // FGETS
        printf("%s", prompt);
        fgets(commandUserEnters, COMMANDSIZE, stdin);  
        // get the length of the command to facilitate changing end character
        stringLength = strlen(commandUserEnters);
        commandUserEnters[stringLength - 1] = '\0';
        // get the first token
        token = strtok(commandUserEnters, delimiter);

        // walk through other tokens
        while(token != NULL){
                int size = i + 1;
                parsed_command = realloc(parsed_command, sizeof(char*) * size);
                parsed_command[i] = malloc(strlen(token) + 1);
                strcpy(parsed_command[i], token);
                i++;
                token = strtok(NULL, delimiter); //remembers last token
                
        }
        // need to allocate space for NULL in order to properly run execvp()
        parsed_command = realloc(parsed_command, sizeof(char*) * (i+1) );
        parsed_command[i] =  NULL;

        //Send the string array of commands and the number of commands in integer form
        //Have to manually keep track of the size of things when malloc is involved. No library call to help.
        int status;
        int child = waitpid(-1, &status, WNOHANG);
        

            if(child > 0){
                if (WIFEXITED(status)) {
                    printf("PID: %d - Child exited with status= %d\n", child, WEXITSTATUS(status));
                } 
                else if (WIFSIGNALED(status)) {
                    printf("Child exited with signal %d\n", WTERMSIG(status));
                }
            }
        runBuiltInCommand(parsed_command, i);
        freeStringArray(parsed_command);    
    }

    return 0;
}

void runBuiltInCommand(char** commandString, int numInputs){

    if(strcmp(commandString[0], "exit") == 0){
        exit(0);
    }
    else if(strcmp(commandString[0], "pwd") == 0){
        printf("%s\n", getcwd(NULL, 0));
    }
    else if(strcmp(commandString[0], "cd") == 0){
        if(commandString[1]){
            chdir(commandString[1]);
        }
        else
        {
            chdir(getenv("HOME"));
        }
    }
    else if(strcmp(commandString[0], "pid") == 0){
        printf("Process ID: %d \n", getpid());
    }
    else if(strcmp(commandString[0], "ppid") == 0){
        printf("Parent process ID: %d \n", getppid());
    }
    // test if user wants to run command in the background
    else if(strcmp(commandString[numInputs - 1], "&") == 0){
        prepareBackgroundExecution(commandString, numInputs);
    }
    else {
        runProgramCommand(commandString);
    }

}

void prepareBackgroundExecution(char** commandString, int numInputs){

    char** copyCommand = malloc(sizeof(char*) * numInputs);
    // pthread_t backgroundThread;

    for(int i = 0; i < numInputs; i++){
        // in the main function we had to allocate one extra index for NULL, but we can just replace the 
        // '&' in this case
        if(i == numInputs - 1){
            copyCommand[i] = NULL;
        }
        else{
            copyCommand[i] = commandString[i];
        }
    }

    executeBackgroundThread(copyCommand);
    
    // pthread_create(&backgroundThread, NULL, (void*)&executeBackgroundThread, &copyCommand);
}

void executeBackgroundThread(char** commandString){

        int status;
        int returnedVal = fork();

        if (returnedVal < 0){
            fprintf(stderr, "fork failed\n");
            exit(1);
        }
        else if(returnedVal == 0){
            // success, inside of child process
            execvp(commandString[0], commandString);

            if(errno != 0){
                printf("error - %s\n", strerror(errno));
            }
            exit(errno);
        }
        else{
            printf("Background ID: %d\n", returnedVal);
        }

    return ;
}

void runProgramCommand(char** commandString){

        int returnedVal = fork();
        int status;

        if (returnedVal < 0){
            fprintf(stderr, "fork failed\n");
            exit(1);
        }
        else if(returnedVal == 0){
            printf("Process ID: %d \n", getpid());
            // success, inside of child process
            execvp(commandString[0], commandString);

            if(errno != 0){
                printf("error - %s\n", strerror(errno));
            }
            
            exit(errno);
        }
        else {
            waitpid(returnedVal, &status, 0);

            if (WIFEXITED(status)) {
                printf("PID: %d - Child exited with status= %d\n", returnedVal, WEXITSTATUS(status));
            } 
            else if (WIFSIGNALED(status)) {
                printf("Child exited with signal %d\n", WTERMSIG(status));
            }
        }

}

void freeStringArray(char** commandStringArray){
    int j = 0;

    while(commandStringArray[j] != NULL){
        free(commandStringArray[j]);
        j++;
    }
    free(commandStringArray);
}