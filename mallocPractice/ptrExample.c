/*Derrick Brandt*/
/*I think I finally understand pointers*/

#include <stdio.h>

int main(){
	int x = 14;
	printf("x equals %d\n", x);

	x = 7;
	printf("x equals %d\n", x);
	
	//Pointers store addresss
	//always follow this format and use the speech below
	//We declare a variable named aptr
	//of type integer pointer
	//it points to an integer
	//and we assign it the memory address of x
	//it holds the address of x

	int* aptr = &x;
	printf("The address of x is %x\n", aptr);
	printf("The address of the pointer using & is: %x\n", &aptr);
	printf("The value of x using the pointer is: %d\n", *aptr);

	//I changed the value using the pointer!!!!
	//If I have the address of a variable, I can do anything!!!	
	*aptr = 99;
	printf("Using the variable x: %d; using the pointer variable: %d \n", x, *aptr);
	return 0;
}
