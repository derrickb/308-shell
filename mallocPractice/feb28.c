/* Derrick Brandt */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char** argv){

    int* int_pointer;
    char* char_pointer;
    int length;

    int_pointer = (int *) malloc(50);
    char_pointer = (char *) malloc(50);

    printf("The start memory address of int_pointer in the heap is: %p\n", int_pointer);
    printf("The memory address int_pointer in the stack is: %p\n", &int_pointer);

    printf("The start memory address char_pointer points to in the heap is: %p\n", char_pointer);
    printf("The memory address char_pointer in the stack is: %p\n", &char_pointer);

    length = (int_pointer + 50) - int_pointer;

    printf("Pointer math? : %d\n", length);

    for(int i = 0; i < length; i++){
        printf("The memory address %p stores int_pointer[%d] \n", (int_pointer + i), i);
    }

    length = (char_pointer + 50) - char_pointer;

    printf("Pointer math? : %d\n", length);

    for(int i = 0; i < length; i++){
        printf("The memory address %p stores char_pointer[%d] \n", (char_pointer + i), i);
    }

    for(int i = 0; i < length; i++){
        *(char_pointer + i) = 'a';
    }

    strcpy((char_pointer + 25), "derrick brandt");

    for(int i = 0; i < length; i++){
        printf("The value at address %p of char_pointer[%d] is %c \n", (char_pointer + i), i, *char_pointer);
    }






    return 0;

}